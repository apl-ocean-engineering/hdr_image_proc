/******************************************************************************
 * Software License Agreement (BSD 3-Clause License)
 *
 * Copyright (C) 2023 University of Washington. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of Magazino GmbH nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

#include "hdr_image_proc/bit_selection.h"

#include <ros/console.h>
#include <stdint.h>

#include "hdr_image_proc/bit_selection.h"

namespace hdr_image_proc {

void TonemapBitSelect::process(cv::InputArray src, cv::OutputArray dst) {
  cv::Mat in = src.getMat();
  auto out = bitSelect(in, shift_);

  if (!out) {
    dst.clear();
    return;
  }

  dst.assign(out.value());
}

// Bit select requires a 32SC3 image, although it treats is as **unsigned**
//
// It extracts the most-significant 8 bits of each channel to produce an 8UC3
// of the same size as the input image.
//
// If provided, it left-shifts each channel by offset first, **with
// saturation.**
//
// e.g. if the value for a given pixel/channel is
//
//   val_32  = 01111000 10101010 10101010 10101010
//
// bitSelect(offset=0) will return 01111000
// bitSelect(offset=1) will return 11110001
// bitSelect(offset=2) will return 11111111 (due to saturation)
//
// Offsets greater than 24 or less than 0 are an error
ExpectMat bitSelect(const cv::Mat &in, int offset) {
  if ((offset < 0) || (offset > (in.elemSize1() - 1) * 8)) {
    return tl::make_unexpected<std::string>("Invalid offset");
  }

  if (in.type() != CV_32SC3) {
    return tl::make_unexpected<std::string>("Unexpected Mat type");
  }

  cv::Mat out(in.size(), CV_8UC3);

  cv::Vec3b pix;

  const int shift = (in.elemSize1() - 1) * 8 - offset;

  // ROS_INFO_STREAM("Shifting by " << shift);

  // Just do it manually for now
  for (unsigned int y = 0; y < in.size().height; y++) {
    for (unsigned int x = 0; x < in.size().width; x++) {
      const cv::Vec<uint32_t, 3> val =
          in.at<cv::Vec<uint32_t, 3> >(cv::Point(x, y));

      for (unsigned int channel = 0; channel < 3; channel++) {
        int32_t p = val[channel] >> shift;

        if (p > 255)
          pix[channel] = 255;
        else
          pix[channel] = (p & 0xFF);
      }

      out.at<cv::Vec3b>(cv::Point(x, y)) = pix;
    }
  }

  return out;
}

}  // namespace hdr_image_proc
