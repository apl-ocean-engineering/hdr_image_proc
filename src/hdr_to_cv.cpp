/******************************************************************************
 * Software License Agreement (BSD 3-Clause License)
 *
 * Copyright (C) 2023 University of Washington. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of Magazino GmbH nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

#include <sensor_msgs/image_encodings.h>

#include "hdr_image_proc/hdr_debayer.h"
#include "hdr_image_proc/hdr_image_encodings.h"

namespace hdr_image_proc {

ExpectMat unpackBayer24(const sensor_msgs::ImageConstPtr &img) {
  if (img->encoding.rfind("bayer", 0)) {
    return tl::make_unexpected<std::string>("Unexpected encoding " +
                                            img->encoding);
  } else if (img->step != img->width * 3) {
    return tl::make_unexpected<std::string>(
        "Unexpected step size for 24bit Bayer data");
  } else if (img->data.size() != (img->width * img->height * 3)) {
    return tl::make_unexpected<std::string>(
        "Unexpected data size for 24bit Bayer data");
  }

  cv::Mat out(cv::Size(img->width, img->height), CV_32SC1);
  auto d = img->data.begin();

  // Just do it manually for now
  for (unsigned int y = 0; y < img->height; y++) {
    for (unsigned int x = 0; x < img->width; x++) {
      int32_t val;

      if (img->is_bigendian) {
        val = (*(d++) << 16) | (*(d++) << 8) | *(d++);
      } else {
        val = *(d++) | (*(d++) << 8) | (*(d++) << 16);
      }

      out.at<uint32_t>(cv::Point(x, y)) = val << 8;
    }
  }

  return out;
}

// \todo{amarburg} This is hacky right now...
ExpectMat unpackRGB24(const sensor_msgs::ImageConstPtr &img) {
  if ((img->encoding != "bgr24") && (img->encoding != "rgb24")) {
    return tl::make_unexpected<std::string>("Unexpected encoding " +
                                            img->encoding);
  }

  // Assume the data is packed [ BBB GGG RRR ] [ BBB GGG RRR ] ... (for bgr24)
  cv::Mat out(cv::Size(img->width, img->height), CV_32SC3);
  auto d = img->data.begin();

  cv::Vec3i pix;

  // Just do it manually for now
  for (unsigned int y = 0; y < img->height; y++) {
    for (unsigned int x = 0; x < img->width; x++) {
      for (unsigned int channel = 0; channel < 3; channel++) {
        uint32_t val;

        if (img->is_bigendian) {
          val = (*(d++) << 16) | (*(d++) << 8) | *(d++);
        } else {
          val = *(d++) | (*(d++) << 8) | (*(d++) << 16);
        }

        // n.b.  Data is MSB-aligned within the 32bits
        // This confirms with other **unpacked** 16 > x > 8 bit
        // formats like mono12 which align the bits to the top
        // of 16bits
        pix[channel] = val << 8;
      }

      out.at<cv::Vec3i>(cv::Point(x, y)) = pix;
    }
  }

  return out;
}

ExpectMat unpackMono16(const sensor_msgs::ImageConstPtr &img) {
  if ((img->encoding != "mono16") && (img->encoding != "mono12") &&
      (img->encoding != "16UC1")) {
    return tl::make_unexpected<std::string>("Unexpected encoding " +
                                            img->encoding);
  }

  cv::Mat out(cv::Size(img->width, img->height), CV_16UC1, cv::Scalar::all(0));
  auto d = img->data.begin();

  // Just do it manually for now
  for (unsigned int y = 0; y < img->height; y++) {
    for (unsigned int x = 0; x < img->width; x++) {
      uint16_t val;

      if (img->is_bigendian) {
        val = (*(d++) << 8) | *(d++);
      } else {
        val = *(d++) | (*(d++) << 8);
      }

      out.at<uint16_t>(cv::Point(x, y)) = val;
    }
  }

  return out;
}

// Replacement for convertTo(..., CV_32FC?) which
// treats CV_32SC? as an **unsigned int**
//
// sigh
cv::Mat toFloatMat(const cv::Mat &in) {
  if (in.type() != CV_32SC3) {
    // \todo should be a cleaner way to do this...
    if (in.channels() == 3) {
      cv::Mat f_mat(in.size(), CV_32FC3);
      in.convertTo(f_mat, CV_32FC1);
      return f_mat;
    } else {
      cv::Mat f_mat(in.size(), CV_32FC1);
      in.convertTo(f_mat, CV_32FC3);
      return f_mat;
    }
  }

  // \todo.  only handles 3-channel data right now

  cv::Mat out(in.size(), CV_32FC3);
  cv::Vec3f pix;

  // Just do it manually for now
  for (unsigned int y = 0; y < in.size().height; y++) {
    for (unsigned int x = 0; x < in.size().width; x++) {
      const cv::Vec<uint32_t, 3> val =
          in.at<cv::Vec<uint32_t, 3>>(cv::Point(x, y));

      for (unsigned int channel = 0; channel < 3; channel++) {
        pix[channel] = val[channel];
      }

      out.at<cv::Vec3f>(cv::Point(x, y)) = pix;
    }
  }

  return out;
}

}  // namespace hdr_image_proc
