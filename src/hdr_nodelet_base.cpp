/******************************************************************************
 * Software License Agreement (BSD 3-Clause License)
 *
 * Copyright (C) 2023 University of Washington. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of Copyright Holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

#include "hdr_image_proc/hdr_nodelet_base.h"

#include <cv_bridge/cv_bridge.h>
#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include "hdr_image_proc/hdr_debayer.h"
#include "hdr_image_proc/hdr_image_encodings.h"
#include "hdr_image_proc/hdr_to_cv.h"

namespace hdr_image_proc {

using cv::Mat;
using cv_bridge::CvImage;

HdrNodeletBase::HdrNodeletBase() : Nodelet() {}

HdrNodeletBase::~HdrNodeletBase() {}

void HdrNodeletBase::onInit() {
  ros::NodeHandle nh = getNodeHandle();
  ros::NodeHandle pnh = getPrivateNodeHandle();

  // in_image_sub_ =
  //     nh.subscribe("image_raw", 1, &HdrNodeletBase::imageCallback, this);

  out_image_pub_ = nh.advertise<sensor_msgs::Image>(
      "image_out", 1,
      boost::bind(&HdrNodeletBase::connectCallback, this,
                  boost::placeholders::_1),
      boost::bind(&HdrNodeletBase::connectCallback, this,
                  boost::placeholders::_1));
}

void HdrNodeletBase::connectCallback(
    const ros::SingleSubscriberPublisher& pub) {
  if (out_image_pub_.getNumSubscribers() == 0) {
    ROS_INFO("No subscribers, unsubscribing from the input stream");
    in_image_sub_ = ros::Subscriber();
  } else if (in_image_sub_.getTopic().empty()) {
    ROS_INFO(
        "A subscriber has connected; need to restart my input subscription");
    in_image_sub_ = getNodeHandle().subscribe(
        "image_raw", 1, &HdrNodeletBase::imageCallback, this);
  }
}

void HdrNodeletBase::imageCallback(const sensor_msgs::ImageConstPtr& img) {
  if (img->encoding == hdr_image_proc::image_encodings::BAYER_RGGB24) {
    auto bayerResult = unpackBayer24(img);
    if (!bayerResult) {
      NODELET_WARN_STREAM(
          "Error unpacking Bayer image: " << bayerResult.error().why());
      return;
    }

    Mat image_32s_rgb = bayerBilinearInt32(bayerResult.value(), img->encoding);
    auto out_mat = handleHDRImg(image_32s_rgb);

    if (out_mat) {
      CvImage out(img->header, encoding(), out_mat.value());
      out_image_pub_.publish(out);
    }

  } else if ((img->encoding == hdr_image_proc::image_encodings::BGR24) ||
             ((img->encoding == hdr_image_proc::image_encodings::RGB24))) {
    auto unpackResult = unpackRGB24(img);
    if (!unpackResult) {
      NODELET_WARN_STREAM(
          "Error unpacking 24-bit image: " << unpackResult.error().why());
      return;
    }

    auto out_mat = handleHDRImg(unpackResult.value());

    if (out_mat) {
      CvImage out(img->header, encoding(), out_mat.value());
      out_image_pub_.publish(out);
    }
  } else if ((img->encoding == hdr_image_proc::image_encodings::MONO12) ||
             (img->encoding == sensor_msgs::image_encodings::MONO16)) {
    int scaler;
    if (img->encoding == hdr_image_proc::image_encodings::MONO12) {
      NODELET_INFO_THROTTLE(10, "Got 12-bit mono");
      scaler = 16;
    } else {
      NODELET_INFO_THROTTLE(10, "Got 16-bit mono");
      scaler = 256;
    }

    auto unpackResult = unpackMono16(img);
    if (!unpackResult) {
      NODELET_WARN_STREAM(
          "Error unpacking 16-bit mono image: " << unpackResult.error().why());
      return;
    }

    // double mn, mx;
    // minMaxLoc( unpackResult.value(), &mn, &mx);
    // NODELET_INFO_STREAM("Mono16 image:  min = " << mn << " ; max = " << mx);

    cv::Mat out16 = unpackResult.value() / scaler;
    cv::Mat out;
    out16.convertTo(out, CV_8UC1);

    // minMaxLoc(out16, &mn, &mx);
    // NODELET_INFO_STREAM("Scaled mono16 image:  min = " << mn << " ; max = "
    // << mx);

    // minMaxLoc(out, &mn, &mx);
    // NODELET_INFO_STREAM("Scaled mono8 image:  min = " << mn
    //                                                    << " ; max = " <<
    //                                                    mx);

    CvImage out_msg(img->header, "mono8", out);
    out_image_pub_.publish(out_msg);

  } else {
    NODELET_INFO_STREAM("Got non-HDR encoding \"" << img->encoding
                                                  << "\"; passing through");
    sensor_msgs::Image out;

    out.header = img->header;
    out.height = img->height;
    out.width = img->width;

    out.is_bigendian = false;
    out.encoding = img->encoding;
    out.step = img->step;

    // Copy?
    out.data = img->data;
    out_image_pub_.publish(out);
  }
}

}  // namespace hdr_image_proc
