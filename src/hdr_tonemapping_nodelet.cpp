/******************************************************************************
 * Software License Agreement (BSD 3-Clause License)
 *
 * Copyright (C) 2023 University of Washington. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of Copyright Holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

#include <cv_bridge/cv_bridge.h>
#include <dynamic_reconfigure/server.h>
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv2/photo.hpp>

#include "hdr_image_proc/ToneMappingConfig.h"
#include "hdr_image_proc/bit_selection.h"
#include "hdr_image_proc/hdr_nodelet_base.h"
#include "hdr_image_proc/hdr_to_cv.h"
#include "hdr_image_proc/tonemap_wrapper.h"

namespace hdr_image_proc {

using cv::Mat;
using cv_bridge::CvImage;

class HdrToneMappingNodelet : public HdrNodeletBase {
 public:
  HdrToneMappingNodelet()
      : HdrNodeletBase(), algo_(TonemapAlgorithm::Linear), gamma_(2.2) {}

  virtual ~HdrToneMappingNodelet() {}

  void onInit() override {
    HdrNodeletBase::onInit();

    ros::NodeHandle nh = getNodeHandle();
    ros::NodeHandle pnh = getPrivateNodeHandle();

    dyn_reconfigure_server_ = std::make_shared<DynReconfigureServer>(pnh);

    dyn_reconfigure_server_->setCallback(
        boost::bind(&HdrToneMappingNodelet::reconfigureCallback, this, _1, _2));
  }

 private:
  // This needs to line up with the Enum in ToneMapping.cfg
  TonemapAlgorithm algo_;

  // Tonemapping parameters
  double gamma_, saturation_, bias_, contrast_scale_;

  ExpectMat handleHDRImg(const cv::Mat &hdr_mat) override {
    // Convert to float

    // n.b. must use our custom float conversion function which
    // passes most typies, but treats treats CV_32SC*  as **unsigned**
    cv::Mat hdr_mat_f = toFloatMat(hdr_mat);

    cv::Mat tonemap_out;
    cv::Ptr<cv::Tonemap> tonemap;
    if (algo_ == TonemapAlgorithm::Linear) {
      tonemap = cv::createTonemap(gamma_);
      NODELET_DEBUG_STREAM_THROTTLE(
          30,
          "Using default linear tonemap with gamma = " << tonemap->getGamma());
    } else if (algo_ == TonemapAlgorithm::Drago) {
      tonemap = cv::createTonemapDrago(gamma_, saturation_, bias_);
      NODELET_DEBUG_STREAM_THROTTLE(30, "Using Drago tonemap with gamma = "
                                            << tonemap->getGamma()
                                            << ", saturation = " << saturation_
                                            << ", bias = " << bias_);

    } else if (algo_ == TonemapAlgorithm::Mantiuk) {
      tonemap = cv::createTonemapMantiuk(gamma_, contrast_scale_, saturation_);
      NODELET_DEBUG_STREAM_THROTTLE(
          30, "Using Mantiuk tonemap with gamma = "
                  << tonemap->getGamma() << ", contrast scale = "
                  << contrast_scale_ << ", saturation = " << saturation_);
    } else if (algo_ == TonemapAlgorithm::Reinhard) {
      tonemap = cv::createTonemapReinhard(gamma_);
      NODELET_DEBUG_STREAM_THROTTLE(
          30, "Using Reinhard tonemap with gamma = " << tonemap->getGamma());
    } else {
      NODELET_WARN_STREAM("Can't handle tonemapping algorithm "
                          << tonemapAlgorithmToString(algo_));
      return tl::make_unexpected<std::string>(
          "Don't understand tonemapping algorithm");
    }

    tonemap->process(hdr_mat_f, tonemap_out);

    // Map back to BGR
    cv::Mat out_bgr;
    tonemap_out.convertTo(out_bgr, CV_8UC3, 255);

    return out_bgr;
  }

  typedef dynamic_reconfigure::Server<hdr_image_proc::ToneMappingConfig>
      DynReconfigureServer;
  std::shared_ptr<DynReconfigureServer> dyn_reconfigure_server_;

  void reconfigureCallback(ToneMappingConfig &config, uint32_t level) {
    algo_ = static_cast<TonemapAlgorithm>(config.algorithm);
    gamma_ = config.gamma;
    saturation_ = config.saturation;
    bias_ = config.bias;
    contrast_scale_ = config.contrast_scale;
  }

  // Non-copyable
  HdrToneMappingNodelet(const HdrToneMappingNodelet &) = delete;
  HdrToneMappingNodelet &operator=(const HdrToneMappingNodelet &) = delete;
};

}  // namespace hdr_image_proc

PLUGINLIB_EXPORT_CLASS(hdr_image_proc::HdrToneMappingNodelet, nodelet::Nodelet)
