/******************************************************************************
 * Software License Agreement (BSD 3-Clause License)
 *
 * Copyright (C) 2023 University of Washington. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of Magazino GmbH nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

#include <cv_bridge/cv_bridge.h>
#include <ros/console.h>
#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <std_msgs/Int32.h>
#include <std_msgs/String.h>

#include <boost/foreach.hpp>
#include <filesystem>
#include <map>
#include <opencv2/highgui.hpp>
#include <string>
#define foreach BOOST_FOREACH

#include "CLI11.hpp"
#include "hdr_image_proc/bit_selection.h"
#include "hdr_image_proc/hdr_debayer.h"
#include "hdr_image_proc/hdr_image_encodings.h"
#include "hdr_image_proc/hdr_to_cv.h"
#include "hdr_image_proc/tonemap_wrapper.h"

// From
// https://answers.ros.org/question/340701/convert-ros-time-to-hours-minutes-seconds-string-in-system-timezone-in-c/
std::string stampToString(const ros::Time &stamp,
                          const std::string format = "%Y-%m-%dT%H:%M:%S") {
  const int output_size = 100;
  char output[output_size];
  std::time_t raw_time = static_cast<time_t>(stamp.sec);

  // Always in GMT
  struct tm *timeinfo = gmtime(&raw_time);
  std::strftime(output, output_size, format.c_str(), timeinfo);
  std::stringstream ss;
  ss << std::setw(9) << std::setfill('0') << stamp.nsec;
  const size_t fractional_second_digits = 6;
  return std::string(output) + "." +
         ss.str().substr(0, fractional_second_digits);
}

using hdr_image_proc::TonemapAlgorithm;

int main(int argc, char **argv) {
  // Define a node name for logging, but not topic mapping right now
  ros::init(std::map<std::string, std::string>(), "reprocess_bag");

  CLI::App app{"App description"};

  std::vector<std::string> bagfiles;
  std::string outdir, image_topic = "/image_raw";
  double gamma = 2.2, saturation = 1.0, bias = 0.85, contrast_scale = 0.7;
  int bit_shift = 0;

  hdr_image_proc::TonemapAlgorithm algo{TonemapAlgorithm::Linear};

  app.add_option("bagfile", bagfiles, "Input bagfiles")->required();
  app.add_option("-o,--output-directory", outdir, "Output directory name")
      ->required();
  app.add_option("--topic", image_topic, "Image topic");

  std::map<std::string, TonemapAlgorithm> map{
      {"bitselect", TonemapAlgorithm::BitSelect},
      {"linear", TonemapAlgorithm::Linear},
      {"drago", TonemapAlgorithm::Drago},
      {"mantiuk", TonemapAlgorithm::Mantiuk},
      {"reinhard", TonemapAlgorithm::Reinhard}};

  app.add_option("-a,--algo", algo, "Algorithm")
      ->transform(CLI::CheckedTransformer(map, CLI::ignore_case));

  // Parameters for algorithms
  app.add_option("--gamma", gamma, "Gamma (used by all algorthms)");
  app.add_option("--saturation", saturation,
                 "Saturation (used by Drago, Mantiuk)");
  app.add_option("--bias", bias, "Bias (used by Drago)");
  app.add_option("--contrast_scale", contrast_scale,
                 "Constrast scale (used by Mantiuk)");

  app.add_option("--shift", bit_shift, "Bit shift for bit select algorithm");

  CLI11_PARSE(app, argc, argv);

  std::filesystem::path output_path(outdir);

  cv::Ptr<cv::Tonemap> tonemap;

  if (algo == hdr_image_proc::TonemapAlgorithm::BitSelect) {
    ROS_INFO_STREAM("Using BitShift algorithm with shift=" << bit_shift);
    tonemap = cv::makePtr<hdr_image_proc::TonemapBitSelect>(bit_shift);
  } else if (algo == hdr_image_proc::TonemapAlgorithm::Linear) {
    ROS_INFO_STREAM("Using Linear algorithm with gamma=" << gamma);
    tonemap = cv::createTonemap(gamma);
  } else if (algo == hdr_image_proc::TonemapAlgorithm::Drago) {
    ROS_INFO("Using Drago algorithm");
    tonemap = cv::createTonemapDrago(gamma, saturation, bias);
  } else if (algo == hdr_image_proc::TonemapAlgorithm::Mantiuk) {
    ROS_INFO("Using Mantiuk algorithm");
    tonemap = cv::createTonemapMantiuk(gamma, contrast_scale, saturation);
  } else if (algo == hdr_image_proc::TonemapAlgorithm::Reinhard) {
    ROS_INFO("Using Reinhard algorithm");
    tonemap = cv::createTonemapReinhard(
        gamma);  // intensity, light_adapt, color_adapt
  }

  hdr_image_proc::TonemapWrapper wrapper(tonemap);

  for (auto const bagfile : bagfiles) {
    ROS_INFO_STREAM("Opening bagfile: " << bagfile << "; looking for topic "
                                        << image_topic);

    rosbag::Bag bag;
    bag.open(bagfile, rosbag::bagmode::Read);

    std::vector<std::string> topics;
    topics.push_back(image_topic);

    rosbag::View view(bag, rosbag::TopicQuery(topics));

    foreach (rosbag::MessageInstance const m, view) {
      sensor_msgs::Image::ConstPtr img = m.instantiate<sensor_msgs::Image>();

      if (img == NULL) continue;

      ROS_INFO_STREAM("Encoding: " << img->encoding);
      cv::Mat out_mat;

      if (hdr_image_proc::isHDR(img->encoding)) {
        cv::Mat image_32s_rgb;

        if (hdr_image_proc::isHDRBayer(img->encoding)) {
          auto bayerResult = hdr_image_proc::unpackBayer24(img);
          if (!bayerResult) {
            ROS_WARN_STREAM(
                "Error unpacking Bayer image: " << bayerResult.error().why());
            continue;
          }

          image_32s_rgb = hdr_image_proc::bayerBilinearInt32(
              bayerResult.value(), img->encoding);
        } else if (hdr_image_proc::isHDR_RGB_BGR(img->encoding)) {
          auto unpackResult = hdr_image_proc::unpackRGB24(img);
          if (!unpackResult) {
            ROS_WARN_STREAM(
                "Error unpacking 24-bit image: " << unpackResult.error().why());
            continue;
          }

          image_32s_rgb = unpackResult.value();
        } else {
          continue;
        }

        cv::Mat tonemap_out;
        wrapper.process(image_32s_rgb, tonemap_out);

        tonemap_out.convertTo(out_mat, CV_8UC3, 255);
        // }

        // if (img->encoding == hdr_image_proc::image_encodings::BAYER_RGGB24) {
        //   auto bayerResult = hdr_image_proc::unpackBayer24(img);
        //   if (!bayerResult) {
        //     ROS_WARN_STREAM(
        //         "Error unpacking Bayer image: " <<
        //         bayerResult.error().why());
        //     continue;
        //   }

        //   cv::Mat image_32s_rgb = hdr_image_proc::bayerBilinearInt32(
        //       bayerResult.value(), img->encoding);

        //   cv::Mat image_32f_rgb = hdr_image_proc::toFloatMat(image_32s_rgb);

        //   cv::Mat tonemap_out;
        //   tonemap->process(image_32f_rgb, tonemap_out);

        //   // cv::Mat out_bgr;
        //   tonemap_out.convertTo(out_mat, CV_8UC3, 255);
        //   // cv::cvtColor(out_bgr, out_mat, cv::COLOR_BGR2RGB);

        // } else if ((img->encoding == hdr_image_proc::image_encodings::BGR24)
        // ||
        //            ((img->encoding ==
        //            hdr_image_proc::image_encodings::RGB24))) {
        //   auto unpackResult = hdr_image_proc::unpackRGB24(img);
        //   if (!unpackResult) {
        //     ROS_WARN_STREAM(
        //         "Error unpacking 24-bit image: " <<
        //         unpackResult.error().why());
        //     continue;
        //   }

        //   cv::Mat image_32f_rgb =
        //       hdr_image_proc::toFloatMat(unpackResult.value());

        //   cv::Mat tonemap_out;
        //   tonemap->process(image_32f_rgb, tonemap_out);

        //   // cv::Mat out_bgr;
        //   tonemap_out.convertTo(out_mat, CV_8UC3, 255);
        //   // cv::cvtColor(out_bgr, out_mat, cv::COLOR_BGR2RGB);

      } else if ((img->encoding == hdr_image_proc::image_encodings::MONO12) ||
                 (img->encoding == sensor_msgs::image_encodings::MONO16)) {
        int scaler;
        if (img->encoding == hdr_image_proc::image_encodings::MONO12) {
          ROS_INFO_THROTTLE(10, "Got 12-bit mono");
          scaler = 16;
        } else {
          ROS_INFO_THROTTLE(10, "Got 16-bit mono");
          scaler = 256;
        }

        auto unpackResult = hdr_image_proc::unpackMono16(img);
        if (!unpackResult) {
          ROS_WARN_STREAM("Error unpacking 16-bit mono image: "
                          << unpackResult.error().why());
          continue;
        }

        // double mn, mx;
        // minMaxLoc( unpackResult.value(), &mn, &mx);
        // ROS_INFO_STREAM("Mono16 image:  min = " << mn << " ; max = " <<
        // mx);

        cv::Mat out16 = unpackResult.value() / scaler;
        out16.convertTo(out_mat, CV_8UC1);

        // minMaxLoc(out16, &mn, &mx);
        // ROS_INFO_STREAM("Scaled mono16 image:  min = " << mn << " ; max
        // = "
        // << mx);

        // minMaxLoc(out, &mn, &mx);
        // ROS_INFO_STREAM("Scaled mono8 image:  min = " << mn
        //                                                    << " ; max = "
        //                                                    << mx);

      } else {
        ROS_INFO_STREAM("Got non-HDR encoding \"" << img->encoding
                                                  << "\"; passing through");
        out_mat = cv_bridge::toCvCopy(img)->image;
      }

      if (!out_mat.empty()) {
        std::string filename("image_");
        filename += stampToString(img->header.stamp) + ".png";

        auto fullpath = (output_path / filename).string();
        ROS_INFO_STREAM("Saving image to " << fullpath);

        cv::imwrite(fullpath, out_mat);
      }
    }

    bag.close();
  }

  exit(0);
}
