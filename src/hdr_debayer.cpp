/******************************************************************************
 * Software License Agreement (BSD 3-Clause License)
 *
 * Copyright (C) 2023 University of Washington. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of Magazino GmbH nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

#include "hdr_image_proc/hdr_debayer.h"

#include <sensor_msgs/image_encodings.h>

#include "hdr_image_proc/hdr_image_encodings.h"

namespace hdr_image_proc {

// Bilinear algorithm reworked from
// https://github.com/jdthomas/bayer2rgb/blob/master/bayer.c
cv::Mat bayerBilinearInt32(const cv::Mat &bayerMat, std::string encoding) {
  assert(bayerMat.type() == CV_32SC1);
  assert(bayerMat.isContinuous());

  cv::Mat outMat(bayerMat.size(), CV_32SC3, cv::Scalar::all(0));
  assert(outMat.isContinuous());

  int width = bayerMat.size().width;
  int height = bayerMat.size().height;

  int bayerStep = width;
  int rgbStep = 3 * width;

  // This is messy since we're reusing existing code
  const uint32_t *bayer = reinterpret_cast<const uint32_t *>(bayerMat.ptr());
  uint32_t *rgb = reinterpret_cast<uint32_t *>(outMat.ptr());

  std::string tile;
  if (encoding == hdr_image_proc::image_encodings::BAYER_RGGB24) {
    tile = "BG";
  }

  // From https://github.com/jdthomas/bayer2rgb/blob/master/bayer.c Bayer tile
  // decision:
  //
  // int blue = tile == DC1394_COLOR_FILTER_BGGR || tile ==
  // DC1394_COLOR_FILTER_GBRG ? -1 : 1; int start_with_green = tile ==
  // DC1394_COLOR_FILTER_GBRG || tile == DC1394_COLOR_FILTER_GRBG;
  //
  // Assume Bayer tile pattern is RGGB:
  // int blue = -1
  // int start_with_green = 0;

  int blue = tile == "BG" || tile == "GB" ? -1 : 1;
  int start_with_green = tile == "GB" || tile == "GR";

  rgb += rgbStep + 3 + 1;
  height -= 2;
  width -= 2;

  for (; height--; bayer += bayerStep, rgb += rgbStep) {
    uint64_t t0, t1;
    const uint32_t *bayerEnd = bayer + width;

    // Lots of casting toe uint64_t so the uin32_t math doesn't overrun...

    if (start_with_green) {
      /* OpenCV has a bug in the next line, which was
      t0 = (bayer[0] + bayer[bayerStep * 2] + 1) >> 1; */
      t0 = ((uint64_t)bayer[1] + bayer[bayerStep * 2 + 1] + 1) >> 1;
      t1 = ((uint64_t)bayer[bayerStep] + bayer[bayerStep + 2] + 1) >> 1;
      rgb[-blue] = (uint32_t)t0;
      rgb[0] = bayer[bayerStep + 1];
      rgb[blue] = (uint32_t)t1;
      bayer++;
      rgb += 3;
    }

    if (blue > 0) {
      for (; bayer <= bayerEnd - 2; bayer += 2, rgb += 6) {
        t0 = ((uint64_t)bayer[0] + bayer[2] + bayer[bayerStep * 2] +
              bayer[bayerStep * 2 + 2] + 2) >>
             2;
        t1 = ((uint64_t)bayer[1] + bayer[bayerStep] + bayer[bayerStep + 2] +
              bayer[bayerStep * 2 + 1] + 2) >>
             2;
        rgb[-1] = (uint32_t)t0;
        rgb[0] = (uint32_t)t1;
        rgb[1] = bayer[bayerStep + 1];

        t0 = ((uint64_t)bayer[2] + bayer[bayerStep * 2 + 2] + 1) >> 1;
        t1 = ((uint64_t)bayer[bayerStep + 1] + bayer[bayerStep + 3] + 1) >> 1;
        rgb[2] = (uint32_t)t0;
        rgb[3] = bayer[bayerStep + 2];
        rgb[4] = (uint32_t)t1;
      }
    } else {
      for (; bayer <= bayerEnd - 2; bayer += 2, rgb += 6) {
        t0 = ((uint64_t)bayer[0] + bayer[2] + bayer[bayerStep * 2] +
              bayer[bayerStep * 2 + 2] + 2) >>
             2;
        t1 = ((uint64_t)bayer[1] + bayer[bayerStep] + bayer[bayerStep + 2] +
              bayer[bayerStep * 2 + 1] + 2) >>
             2;
        rgb[1] = (uint32_t)t0;
        rgb[0] = (uint32_t)t1;
        rgb[-1] = bayer[bayerStep + 1];

        t0 = ((uint64_t)bayer[2] + bayer[bayerStep * 2 + 2] + 1) >> 1;
        t1 = ((uint64_t)bayer[bayerStep + 1] + bayer[bayerStep + 3] + 1) >> 1;
        rgb[4] = (uint32_t)t0;
        rgb[3] = bayer[bayerStep + 2];
        rgb[2] = (uint32_t)t1;
      }
    }

    if (bayer < bayerEnd) {
      t0 = ((uint64_t)bayer[0] + bayer[2] + bayer[bayerStep * 2] +
            bayer[bayerStep * 2 + 2] + 2) >>
           2;
      t1 = ((uint64_t)bayer[1] + bayer[bayerStep] + bayer[bayerStep + 2] +
            bayer[bayerStep * 2 + 1] + 2) >>
           2;
      rgb[-blue] = (uint32_t)t0;
      rgb[0] = (uint32_t)t1;
      rgb[blue] = bayer[bayerStep + 1];
      bayer++;
      rgb += 3;
    }

    bayer -= width;
    rgb -= width * 3;

    blue = -blue;
    start_with_green = !start_with_green;
  }

  return outMat;
}
}  // namespace hdr_image_proc
