/******************************************************************************
 * Software License Agreement (BSD 3-Clause License)
 *
 * Copyright (C) 2023 University of Washington. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of Magazino GmbH nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

#include <cv_bridge/cv_bridge.h>
#include <dynamic_reconfigure/server.h>
#include <nodelet/nodelet.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv2/photo.hpp>

#include "hdr_image_proc/BitSelectionConfig.h"
#include "hdr_image_proc/bit_selection.h"
#include "hdr_image_proc/hdr_nodelet_base.h"

namespace hdr_image_proc {

using cv::Mat;
using cv_bridge::CvImage;

class HdrBitSelectionNodelet : public HdrNodeletBase {
 public:
  HdrBitSelectionNodelet();
  virtual ~HdrBitSelectionNodelet();

  void onInit() override;

 private:
  int offset_;

  ExpectMat handleHDRImg(const cv::Mat &hdr_mat) override;

  typedef dynamic_reconfigure::Server<hdr_image_proc::BitSelectionConfig>
      DynReconfigureServer;
  std::shared_ptr<DynReconfigureServer> dyn_reconfigure_server_;
  void reconfigureCallback(BitSelectionConfig &config, uint32_t level);

  // Non-copyable
  HdrBitSelectionNodelet(const HdrBitSelectionNodelet &) = delete;
  HdrBitSelectionNodelet &operator=(const HdrBitSelectionNodelet &) = delete;
};

HdrBitSelectionNodelet::HdrBitSelectionNodelet()
    : HdrNodeletBase(), offset_(0) {}

HdrBitSelectionNodelet::~HdrBitSelectionNodelet() {}

void HdrBitSelectionNodelet::onInit() {
  HdrNodeletBase::onInit();

  ros::NodeHandle nh = getNodeHandle();
  ros::NodeHandle pnh = getPrivateNodeHandle();

  dyn_reconfigure_server_ = std::make_shared<DynReconfigureServer>(pnh);

  dyn_reconfigure_server_->setCallback(
      boost::bind(&HdrBitSelectionNodelet::reconfigureCallback, this, _1, _2));
}

ExpectMat HdrBitSelectionNodelet::handleHDRImg(const cv::Mat &hdr_mat) {
  auto selectResult = bitSelect(hdr_mat, offset_);
  if (!selectResult) {
    NODELET_WARN_STREAM("Error bit selecting: " << selectResult.error().why());
    return tl::make_unexpected(selectResult.error());
  }

  return selectResult.value();
}

void HdrBitSelectionNodelet::reconfigureCallback(BitSelectionConfig &config,
                                                 uint32_t level) {
  if (config.offset != offset_) {
    offset_ = config.offset;
    NODELET_INFO_STREAM("Updating offset to " << offset_);
  }
}

}  // namespace hdr_image_proc

PLUGINLIB_EXPORT_CLASS(hdr_image_proc::HdrBitSelectionNodelet, nodelet::Nodelet)
