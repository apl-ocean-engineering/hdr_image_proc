ARG ROS_VERSION=noetic

#====================================================================
# Preliminary image with dependencies
#====================================================================
#
## This should work on both amd64 and arm64 through the magic of multi-arch?
FROM ros:${ROS_VERSION}-perception AS deps

RUN apt-get update && apt-get install -y \
    git \
    python3-catkin-tools \
    python3-vcstool \
    && apt autoremove -y \
    && apt clean -y \
    && rm -rf /var/lib/apt/lists/*

#====================================================================
# Intermediate image with prereq steps for running in CI
#====================================================================
FROM deps as ci

## ~~ Create non-root user ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
ARG USERNAME=ros
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN groupadd --gid $USER_GID $USERNAME \
  && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
  #
  # [Optional] Add sudo support. Omit if you don't need to install software after connecting.
  && apt-get update \
  && apt-get install -y sudo \
  && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
  && chmod 0440 /etc/sudoers.d/$USERNAME

USER $USERNAME

# Install the default ROS entrypoint
COPY --chmod=0755 ros_entrypoint.sh /home/$USERNAME/ros_entrypoint.sh
ENTRYPOINT [ "/home/ros/ros_entrypoint.sh" ]
CMD ["/bin/bash"]

#====================================================================
# Intermediate image with cloned Git repos
#====================================================================
FROM ci as build_clone

ARG WS_DIR=/home/$USERNAME/ros_ws
ONBUILD WORKDIR ${WS_DIR}/src

ARG HDR_IMAGE_PROC_ROS_REPO=https://gitlab.com/apl-ocean-engineering/hdr_image_proc.git
ARG HDR_IMAGE_PROC_ROS_BRANCH=main
ONBUILD RUN echo "Cloning from ${HDR_IMAGE_PROC_ROS_BRANCH} branch ${HDR_IMAGE_PROC_ROS_REPO}"

# This will break cache when the repo changes
ONBUILD RUN git clone --depth 1 -b ${HDR_IMAGE_PROC_ROS_BRANCH} ${HDR_IMAGE_PROC_ROS_REPO}

#====================================================================
# Final image with built ROS packages
#====================================================================
# dockerfile_lint - ignore
FROM build_clone as hdr_image_proc

ARG WS_DIR
LABEL Version=0.1
LABEL Name=hdr_image_proc

WORKDIR ${WS_DIR}

# This is quite expensive to run on every build...
# RUN bash -c "apt-get update \
#               && source /opt/ros/noetic/setup.bash \
#               && rosdep install -y --ignore-src \
#                     --skip-keys=arena_sdk \
#                     --from-paths src/ \
#               && rm -rf /var/lib/apt/lists/*"

RUN bash -c "~/ros_entrypoint.sh \
                catkin build"

# Convert ARG to ENV
ENV ROS_WS ${WS_DIR}
