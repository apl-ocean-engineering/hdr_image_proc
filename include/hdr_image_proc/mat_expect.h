#pragma once

#include <opencv2/core.hpp>

#include "hdr_image_proc/expected.hpp"

namespace hdr_image_proc {

class Error {
 public:
  Error(const std::string &why) : why_(why) {}

  const std::string &why() const { return why_; }

  std::string why_;
};

using ExpectMat = tl::expected<cv::Mat, Error>;

}  // namespace hdr_image_proc
