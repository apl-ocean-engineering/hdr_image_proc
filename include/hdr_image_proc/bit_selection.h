/******************************************************************************
 * Software License Agreement (BSD 3-Clause License)
 *
 * Copyright (C) 2023 University of Washington. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of Magazino GmbH nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/

#pragma once

#include <opencv2/core.hpp>
#include <opencv2/photo.hpp>

#include "hdr_image_proc/mat_expect.h"

namespace hdr_image_proc {

// n.b. This is a good idea in principle, but the existing cv::Tonemap
// algorithms expect a 32F image, while BitSelect requires as 32S.
// So.... we need another wrapper (TonemapWrapper) to perform appropriate
// image pre-conversion to make it work transparently in userspace.  Icky.
//
class TonemapBitSelect : public cv::Tonemap {
 public:
  TonemapBitSelect(int shift = 0) : shift_(shift) { ; }

  // Gamma isn't used by this algorithm
  float getGamma() const override { return 0; }
  void setGamma(float gamma) override { ; }

  void process(cv::InputArray src, cv::OutputArray dst) override;

 protected:
  int shift_;
};

ExpectMat bitSelect(const cv::Mat &mat, int shift);

}  // namespace hdr_image_proc
