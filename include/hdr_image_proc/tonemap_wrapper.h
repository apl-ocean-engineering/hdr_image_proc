#pragma once

#include <opencv2/photo.hpp>

#include "hdr_image_proc/bit_selection.h"
#include "hdr_image_proc/hdr_to_cv.h"

namespace hdr_image_proc {

// These needs to line up with the values for tonemap_algo_enum in
// ToneMapping.cfg
enum class TonemapAlgorithm : int {
  Linear = 0,
  Drago = 1,
  Mantiuk = 2,
  Reinhard = 3,
  BitSelect = 99
};

inline TonemapAlgorithm stringToTonemapAlgorithm(const std::string &str) {
  if (str == "linear") {
    return TonemapAlgorithm::Linear;
  } else if (str == "drago") {
    return TonemapAlgorithm::Drago;
  } else if (str == "mantiuk") {
    return TonemapAlgorithm::Mantiuk;
  } else if (str == "reinhard") {
    return TonemapAlgorithm::Reinhard;
  } else {
    // How to handle error
  }

  return TonemapAlgorithm::Linear;
}

inline const std::string tonemapAlgorithmToString(const TonemapAlgorithm algo) {
  if (algo == TonemapAlgorithm::Linear) {
    return "Linear";
  } else if (algo == TonemapAlgorithm::Drago) {
    return "Drago";
  } else if (algo == TonemapAlgorithm::Mantiuk) {
    return "Mantiuk";
  } else if (algo == TonemapAlgorithm::Reinhard) {
    return "Reinhard";
  }

  return "(unknown)";
}

/// TonemapWrapper is a convenience abstraction wrapper which provides a
/// consistent interface between OpenCV's tonemap algorithsm and BitSelect,
/// despite the fact that OpenCV"s algorithms take a float mat and BitSelect
/// takes an integer mat
///
/// TonemapWrapper assumes the input mat is an integer type and casts the mat to
/// float if required.
///
class TonemapWrapper {
 public:
  TonemapWrapper(const cv::Ptr<cv::Tonemap> &algo) : algo_(algo) { ; }

  void process(cv::InputArray src, cv::OutputArray dst) {
    cv::Ptr<hdr_image_proc::TonemapBitSelect> as_tone_map =
        algo_.dynamicCast<hdr_image_proc::TonemapBitSelect>();

    if (as_tone_map) {
      algo_->process(src, dst);
    } else {
      algo_->process(toFloatMat(src.getMat()), dst);
    }
  }

 protected:
  cv::Ptr<cv::Tonemap> algo_;
};

}  // namespace hdr_image_proc
