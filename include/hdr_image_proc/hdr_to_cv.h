#pragma once

#include <sensor_msgs/Image.h>

#include <opencv2/core.hpp>

#include "hdr_image_proc/mat_expect.h"

namespace hdr_image_proc {

ExpectMat unpackBayer24(const sensor_msgs::ImageConstPtr &img);

// Technically this doesn't care about the color order (BGR versus RGB)
ExpectMat unpackRGB24(const sensor_msgs::ImageConstPtr &img);

// Expects an img with encoding of "Mono16" or "16UC1"
// Returns a cv::Mat of type CV_16UC1
ExpectMat unpackMono16(const sensor_msgs::ImageConstPtr &img);

// Converts in to the equivalent float/CV_32FC? format
// **except** treating CV_32SC? data as **unsigned**
cv::Mat toFloatMat(const cv::Mat &in);

}  // namespace hdr_image_proc
