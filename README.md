# Packages related to the processing of HDR images

These packages are for the processing and handling of HDR images, particularly the 24-bit-per-color images created by LucidVision [TRI054S](https://thinklucid.com/product/triton-5-mp-imx490/) camera based on the Sony IMX490 sensor using the [arena_camera](https://github.com/apl-ocean-engineering/arena_camera_ros) driver.  That sensor/driver produces `sensor_msgs::Image` messages with the following non-standard `image_encoding` strings:

* `bayer_rggb24`:  An HDR Bayer image with a 3-byte unsigned integer for each pixel.  Total data size is `height * width * 3` bytes.
* `rgb24` and `bgr24`:   A de-Bayered HDR image with a 3-byte unsigned integer for each channel, for each pixel.  Total data size is `height * width * 9` bytes.

This package contains code for unpacking those 24-bit formats into OpenCV `CV_32SC1` and `CV_32SC3` integer format, `CV_32FC1` and `CV_32FC3` floating point formats.   Data is aligned to the **MSB** of the 32-bytes .. this conforms to the behavior of V4L formats like `mono12` which return 12 bits of data aligned to the MSB of 16 bits.

It also handles images with the `mono16` (`CV_16UC1`) format.

Currently, two-nodelets are defined.

* `hdr_image_proc/HdrBitSelectionNodelet`:  Subscribes to an HDR `image_raw` and published an `rgb8` `image_out`.   Performs bit-selection within each pixel based on the ros parame `offset`.   For 32-bit data, `offset=0` it returns `hdr_pix[31:24]`, for `offset=1` it returns `hdr_pix[30:23]` etc.  For 16-bit data it returns `hdr_pix[15:8]` etc.
* `hdr_image_proc/HdrToneMappingNodelet`:  Subscribes to an HDR `image_raw` and published an `rgb8` `image_out`.   Uses the [HDR tonemapping](https://docs.opencv.org/4.x/d6/df5/group__photo__hdr.html#gabcbd653140b93a1fa87ccce94548cd0d) to produce an 8-bit image from the HDR input.   Tonemapping algorithm and parameters can be controlled through ROS params.

# License

This code is released under the [BSD 3-Clause License](LICENSE).

Includes the implementation of [expected](https://github.com/TartanLlama/expected) from [TartanLlama](https://github.com/TartanLlama) which is released under the [CC0-1.0 license](https://github.com/TartanLlama/expected/blob/master/COPYING).
